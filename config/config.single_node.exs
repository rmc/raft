# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
import Config

config :raft,
  id: "s1",
  peers: [
    [id: "s2", type: :same_node, options: []],
    [id: "s3", type: :same_node, options: []],
    [id: "s4", type: :same_node, options: []],
    [id: "s5", type: :same_node, options: []]
  ]
