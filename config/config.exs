# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
import Config

config :raft,
  minimum_election_timeout_duration: 150,
  maximum_election_timeout_duration: 300

config =
  case System.get_env("RAFT_CONFIG") do
    "" ->
      "single_node"

    nil ->
      "single_node"

    config ->
      config
  end

import_config "config.#{config}.exs"
