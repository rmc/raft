# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
import Config

config :raft,
  id: "s4",
  peers: [
    [id: "s1", type: :node, options: [destination: {:s1, :"s1@0.0.0.0"}]],
    [id: "s2", type: :node, options: [destination: {:s2, :"s2@0.0.0.0"}]],
    [id: "s3", type: :node, options: [destination: {:s3, :"s3@0.0.0.0"}]],
    [id: "s5", type: :node, options: [destination: {:s5, :"s5@0.0.0.0"}]]
  ]
