# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
import Config

config :raft,
  id: "s5",
  peers: [
    [id: "s1", type: :node, options: [destination: {:s1, :"s1@0.0.0.0"}]],
    [id: "s2", type: :node, options: [destination: {:s2, :"s2@0.0.0.0"}]],
    [id: "s3", type: :node, options: [destination: {:s3, :"s3@0.0.0.0"}]],
    [id: "s4", type: :node, options: [destination: {:s4, :"s4@0.0.0.0"}]]
  ]
