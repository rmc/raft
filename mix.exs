defmodule Raft.MixProject do
  use Mix.Project

  def project do
    [
      app: :raft,
      version: "0.1.0",
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Raft.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [{:mix_test_watch, "~> 1.1", [only: :dev, runtime: false]}]
  end
end
