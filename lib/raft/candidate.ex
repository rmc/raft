defmodule Raft.Server.Candidate do
  @moduledoc """
  Provides behavior for a Raft Server in Candidate state

  * On conversion to candidate, start election:
    * Increment current_term
    * Vote for self
    * Reset election timer
    * Send RequestVote RPCs to all other servers
  * If votes received from majority of servers: become leader
  * If AppendEntries RPC received from new leader: convert to follower
  * If election timeout elapses: start new election
  """
  require Logger

  alias Raft.Server.{PersistentState, Peer, Follower, Leader}
  alias Raft.Rpc.{AppendEntries, RequestVote}

  def init(
        %{
          persistent_state: persistent_state,
          timers: timers,
          id: id,
          task_supervisor: task_supervisor
        } = state
      ) do
    PersistentState.increment_current_term(persistent_state)
    PersistentState.set_voted_for(persistent_state, id)
    vote_count = 1

    if Timer.running?(timers, :election_timeout, self()) do
      :ok = Timer.restart_timer(timers, :election_timeout, self())
    else
      :ok = Timer.start_timer(timers, :election_timeout, self())
    end

    current_term = Raft.Server.PersistentState.current_term(persistent_state)

    Logger.debug(fn ->
      "Raft: Server Candidate #{inspect(state.id)} starting election for term #{inspect(current_term)}}"
    end)

    if Map.has_key?(state, :candidate_state) do
      cancel_running_election(state)
    end

    Map.put(
      state,
      :candidate_state,
      {request_votes(
         %RequestVote.Request{
           term: current_term,
           candidate_id: id,
           last_log_index: 0,
           last_log_term: 0
         },
         state,
         task_supervisor
       ), vote_count}
    )
  end

  def handle_call({:command, _}, _from, state) do
    {:reply, {:error, "Raft Candidate: unknown leader"}, state}
  end

  def handle_call(
        {:append_entries, %AppendEntries.Request{term: term, leader_id: leader_id}} = request,
        _from,
        state
      ) do
    current_term = PersistentState.current_term(state.persistent_state)

    if term >= current_term do
      Logger.debug(fn ->
        "Raft: Server Candidate #{inspect(state.id)} with term: #{inspect(current_term)} got append entries from #{inspect(leader_id)} with newer term: #{inspect(term)}. Converting to Follower and delegating call"
      end)

      state = cleanup(state)
      {:noreply, state, :convert_and_call, Follower, request}
    else
      Logger.debug(fn ->
        "Raft: Server Candidate #{inspect(state.id)} with term: #{inspect(current_term)} got append entries from #{inspect(leader_id)} with term: #{inspect(term)}. Fail."
      end)

      response = %AppendEntries.Response{
        term: current_term,
        success: false
      }

      {:reply, response, state}
    end
  end

  def handle_call(
        {:request_vote, %RequestVote.Request{candidate_id: candidate_id, term: term}} = request,
        _from,
        %{
          id: id,
          persistent_state: persistent_state
        } = state
      ) do
    current_term = Raft.Server.PersistentState.current_term(persistent_state)

    if term >= current_term do
      Logger.debug(fn ->
        "Raft: Server Candidate #{inspect(id)} with term: #{inspect(current_term)} got request vote from: #{inspect(candidate_id)} for term: #{inspect(term)}. Converting to follower and delegating request"
      end)

      state = cleanup(state)
      {:noreply, state, :convert_and_call, Follower, request}
    else
      response = %RequestVote.Response{
        term: current_term,
        vote_granted: false
      }

      {:reply, response, state}
    end
  end

  def handle_info({:election_timeout, _}, state) do
    {:noreply, init(state)}
  end

  def handle_info(
        {_,
         %RequestVote.Response{
           term: term,
           vote_granted: vote_granted
         }},
        %{
          id: id,
          persistent_state: persistent_state,
          candidate_state: {election_tasks, vote_count},
          peers: peers
        } = state
      ) do
    current_term = PersistentState.current_term(persistent_state)

    Logger.debug(fn ->
      "Raft: Server Candidate #{inspect(id)} got vote_granted: #{inspect(vote_granted)}"
    end)

    if vote_granted && current_term == term do
      vote_count = vote_count + 1

      Logger.debug(fn ->
        "Raft: Server Candidate #{inspect(id)} vote count: #{inspect(vote_count)}"
      end)

      if vote_count == length(peers) / 2 + 1 do
        Logger.debug(fn ->
          "Raft: Server Candidate #{inspect(id)} got majority of votes. Converting to Leader"
        end)

        {:noreply, cleanup(state), :convert, Leader}
      else
        {:noreply, %{state | candidate_state: {election_tasks, vote_count}}}
      end
    else
      {:noreply, state}
    end
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  defp request_votes(request, %{peers: peers}, task_supervisor) do
    Enum.map(peers, fn peer ->
      Task.Supervisor.async_nolink(task_supervisor, fn ->
        do_request = fn retry ->
          try do
            Peer.request_vote(peer, request)
          catch
            _, _ ->
              Process.sleep(100)
              retry.(retry)
          end
        end

        do_request.(do_request)
      end)
    end)
  end

  defp cleanup(%{timers: timers} = state) do
    :ok = Timer.stop_timer(timers, :election_timeout, self())

    cancel_running_election(state)
    Map.delete(state, :candidate_state)
  end

  defp cancel_running_election(%{candidate_state: {election_tasks, _}}) do
    Enum.each(election_tasks, fn task ->
      Task.shutdown(task, :brutal_kill)
    end)
  end
end
