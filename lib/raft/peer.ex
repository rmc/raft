# TODO: Handle call timeouts
# TODO: Implement Http Peer

defmodule Raft.Server.Peer.Native do
  @moduledoc """

  Provides an interface to communicate with other local or remote `Raft.Server` servers
  by delegating the arguments of each call to the `destination` `Raft.Server`
  using a naive retry behavior where it retries each call indefinetly until
  the call timeout (default of 5000ms).
  """

  use GenServer

  def start_link(name: name, destination: destination) do
    GenServer.start_link(__MODULE__, {name, destination}, name: name)
  end

  def command(peer, command) do
    GenServer.call(peer, {:command, command})
  end

  def append_entries(peer, request) do
    GenServer.call(peer, {:append_entries, request})
  end

  def request_vote(peer, request) do
    GenServer.call(peer, {:request_vote, request})
  end

  def init({name, destination}) do
    {:ok, task_supervisor} =
      Task.Supervisor.start_link(name: "#{name}-task-supervisor" |> String.to_atom())

    {:ok, %{destination: destination, task_supervisor: task_supervisor}}
  end

  def handle_call({:command, command}, _from, state) do
    do_request = fn retry ->
      try do
        Raft.Server.command(state.destination, command)
      catch
        _, _ ->
          Process.sleep(100)
          retry.(retry)
      end
    end

    response = do_request.(do_request)
    {:reply, response, state}
  end

  def handle_call({:request_vote, request}, _from, state) do
    do_request = fn retry ->
      try do
        Raft.Server.request_vote(state.destination, request)
      catch
        _, _ ->
          Process.sleep(100)
          retry.(retry)
      end
    end

    response = do_request.(do_request)
    {:reply, response, state}
  end

  def handle_call({:append_entries, request}, _from, state) do
    try do
      response = Raft.Server.append_entries(state.destination, request)
      {:reply, {:ok, response}, state}
    catch
      a, b ->
        {:reply, {:error, a, b}, state}
    end
  end
end

defmodule Raft.Server.Peer.Http do
end

defmodule Raft.Server.Peer do
  @moduledoc """

  Provides an interface to communicate with other servers abstracting
  transport details. See `Raft.Application` and `Raft.Server.Supervisor` for configuration.

  Each peer contains a module which implements the peer behavior.
  """
  def id([{:id, id} | _]) do
    id
  end

  def command([id: _, name: name, type: _, module: module, options: _], command) do
    module.command(name, command)
  end

  def request_vote([id: _, name: name, type: _, module: module, options: _], request) do
    module.request_vote(name, request)
  end

  def append_entries([id: _, name: name, type: _, module: module, options: _], request) do
    module.append_entries(name, request)
  end
end
