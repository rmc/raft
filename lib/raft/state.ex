defmodule Raft.Types do
  @type raft_id :: String.t()
  @type raft_term :: integer()
  @type log_item :: {term :: raft_term(), command :: any(), index :: integer()}
  @type log :: [log_item()]
end

defmodule Raft.Server.Log do
  require Logger

  def new do
    [{0, "_", 0}]
  end

  @spec append_items(Raft.Types.log(), [Raft.Types.log_item()]) :: Raft.Types.log()
  def append_items(log, []) do
    log
  end

  def append_items(log, items) do
    if Enum.all?(items, &is_item_valid?(log, &1)) do
      items =
        Enum.drop_while(items, fn {term, _, index} -> item_with_term_at?(log, index, term) end)

      log = log ++ items
      Logger.debug(fn -> "Raft: Log updated #{inspect(log)}" end)
      log
    end
  end

  defp is_item_valid?(log, {term, _, index}) do
    case Enum.at(log, index) do
      {^term, _, ^index} -> true
      nil -> true
      _ -> false
    end
  end

  def add_item(log, {term, command}) do
    log = log ++ [{term, command, length(log)}]
    Logger.debug(fn -> "Raft: Log updated #{inspect(log)}" end)
    log
  end

  @spec item_with_term_at?(Raft.Types.log(), integer(), Raft.Types.raft_term()) :: boolean()
  def item_with_term_at?(log, index, term) do
    case Enum.at(log, index) do
      {^term, _, ^index} -> true
      _ -> false
    end
  end

  def item_term_at(_, 0) do
    0
  end

  def item_term_at(log, index) do
    case Enum.at(log, index) do
      {item_term, _, ^index} -> item_term
    end
  end

  def items(log, start_index, end_index \\ nil)

  def items(log, 0, end_index) do
    items(log, 1, end_index)
  end

  def items(log, start_index, end_index) do
    end_index =
      cond do
        end_index ->
          end_index

        true ->
          length(log)
      end

    Enum.slice(log, start_index..end_index)
  end

  def last_index(log) do
    length(log) - 1
  end
end

defmodule Raft.Server.PersistentState do
  @moduledoc """

  Must kept on stable storage and updated before responding to RPCs

  * `:current_term` - latest term server has seen (initialized to 0 on first boot, increases monotonically)
  * `:voted_for` - candidateId that received vote in current term (or null if none)
  * `:log` - log entries; each entry contains command for state machine, and term when entry was received by leader (first index is 1)

  """
  use Agent
  alias Raft.Server.Log

  @type state :: %{
          voted_for: Raft.Types.raft_id(),
          current_term: Raft.Types.raft_term(),
          log: Raft.Types.log()
        }

  defstruct [:voted_for, current_term: 0, log: []]

  def start_link(options \\ []) do
    ## TODO: read persisted state if any else create new
    ## TODO: persist index of last committed entry
    voted_for = nil
    current_term = 0
    log = Log.new()

    Agent.start_link(
      fn ->
        %{
          voted_for: voted_for,
          current_term: current_term,
          log: log
        }
      end,
      options
    )
  end

  def current_term(persistent_state) do
    Agent.get(persistent_state, fn %{current_term: current_term} -> current_term end)
  end

  def increment_current_term(persistent_state) do
    Agent.update(persistent_state, fn %{current_term: current_term} = state ->
      %{state | current_term: current_term + 1, voted_for: nil}
    end)
  end

  def voted_for(persistent_state) do
    Agent.get(persistent_state, fn %{voted_for: voted_for} -> voted_for end)
  end

  def set_voted_for(persistent_state, voted_for) do
    Agent.update(persistent_state, fn state -> %{state | voted_for: voted_for} end)
  end

  def set_current_term_and_append_entries(persistent_state, current_term, entries) do
    Agent.update(persistent_state, fn state ->
      %{
        state
        | current_term: current_term,
          voted_for: nil,
          log: Log.append_items(state.log, entries)
      }
    end)
  end

  def set_current_term(persistent_state, current_term) do
    Agent.update(persistent_state, fn state ->
      %{state | current_term: current_term, voted_for: nil}
    end)
  end

  def set_current_term_and_voted_for(persistent_state, current_term, voted_for) do
    Agent.update(persistent_state, fn state ->
      %{state | current_term: current_term, voted_for: voted_for}
    end)
  end

  def append_command(persistent_state, command) do
    persistent_state
    |> Agent.get_and_update(fn state ->
      log = Log.add_item(state.log, {state.current_term, command})
      {Log.last_index(log), %{state | log: log}}
    end)
  end

  def append_entries(persistent_state, entries) do
    persistent_state
    |> Agent.get_and_update(fn state ->
      log = Log.append_items(state.log, entries)
      {Log.last_index(log), %{state | log: log}}
    end)
  end

  def last_index(persistent_state) do
    Agent.get(persistent_state, fn %{log: log} -> Log.last_index(log) end)
  end

  def entries(persistent_state, start_index, end_index \\ nil) do
    Agent.get(persistent_state, fn %{log: log} -> Log.items(log, start_index, end_index) end)
  end

  def term_of_entry_at(persistent_state, index) do
    Agent.get(persistent_state, fn %{log: log} -> Log.item_term_at(log, index) end)
  end

  def item_with_term_at?(persistent_state, index, term) do
    Agent.get(persistent_state, fn %{log: log} -> Log.item_with_term_at?(log, index, term) end)
  end
end

defmodule Raft.Server.VolatileState do
  @moduledoc """

  Reinitialized after election

  * `:commit_index` - index of highest log entry known to be committed (initialized to 0, increases monotonically)
  * `:last_applied` - index of highest log entry applied to state machine (initialized to 0, increases monotonically)

  """

  @type t :: %__MODULE__{
          commit_index: integer(),
          last_applied: integer()
        }

  defstruct commit_index: 0, last_applied: 0

  def init(persistent_state) do
    last_log_index = Raft.Server.PersistentState.last_index(persistent_state)

    %__MODULE__{
      commit_index: last_log_index,
      last_applied: last_log_index
    }
  end
end
