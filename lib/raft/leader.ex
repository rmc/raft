defmodule Raft.Server.Leader do
  @moduledoc """

  Provides behavior for a Raft Server in Leader state
   - Handles client commands by
     - appending the command to the log
     - ensuring that the command was committed to the majority of followers ---| async, retry
     - applying the command to the state machine
     - replying to the client

  Sends AppendEntries RPCs to followers trying to make the followers log match it's own.

  * Upon election: send initial empty AppendEntries RPCs (heartbeat) to each server; repeat during idle periods to prevent election timeouts (§5.2)
  * If command received from client: append entry to local log, respond after entry applied to state machine (§5.3)
  * If last log index ≥ nextIndex for a follower: send AppendEntries RPC with log entries starting at nextIndex
  * If successful: update nextIndex and matchIndex for follower (§5.3)
  * If AppendEntries fails because of log inconsistency: decrement nextIndex and retry (§5.3)
  * If there exists an N such that N > commit_index, a majority of matchIndex[i] ≥ N, and log[N].term == current_term: set commit_index = N (§5.3, §5.4).

  ### State

  * `next_index` - for each server, index of the next log entry to send to that server (initialized to leader last log index + 1)
  * `match_index` - for each server, index of highest log entry known to be replicated on server (initialized to 0, increases monotonically)
  * `pending_commands` - keeps the command callers in order to reply once the command has been persisted to the majority
  * `tasks` - keeps the tasks references in order to stop them on cleanup
  """

  require Logger
  alias Raft.Server.{PersistentState, Peer, Follower}
  alias Raft.Rpc.{AppendEntries, RequestVote}

  def init(%{id: id, persistent_state: persistent_state} = state) do
    Logger.info(fn ->
      current_term = PersistentState.current_term(persistent_state)
      "Raft: Server Leader #{inspect(id)} for term #{inspect(current_term)}"
    end)

    :ok = Timer.start_timer(state.timers, :heartbeat_timeout, self())

    last_log_index = PersistentState.last_index(persistent_state)

    next_index =
      state.peers
      |> Enum.map(fn peer -> {Peer.id(peer) |> String.to_atom(), last_log_index + 1} end)
      |> Map.new()

    match_index =
      state.peers
      |> Enum.map(fn peer -> {Peer.id(peer) |> String.to_atom(), 0} end)
      |> Map.new()

    state =
      Map.put(state, :leader_state, %{
        next_index: next_index,
        match_index: match_index,
        pending_commands: [],
        tasks: []
      })

    send_append_entries(state)
    state
  end

  def handle_call(
        {:request_vote, %RequestVote.Request{term: term, candidate_id: candidate_id}} = request,
        _from,
        state
      ) do
    current_term = PersistentState.current_term(state.persistent_state)

    if term > current_term do
      Logger.debug(fn ->
        "Raft: Server Leader #{inspect(state.id)} with term: #{inspect(current_term)} got request vote from #{inspect(candidate_id)} with newer term: #{inspect(term)}. Converting to Follower and delegating call"
      end)

      state = cleanup(state)
      {:noreply, state, :convert_and_call, Follower, request}
    else
      Logger.debug(fn ->
        "Raft: Server Leader #{inspect(state.id)} with term: #{inspect(current_term)} got request vote from #{inspect(candidate_id)} with term: #{inspect(term)}. Vote not granted."
      end)

      response = %RequestVote.Response{vote_granted: false, term: current_term}
      {:reply, response, state}
    end
  end

  def handle_call(
        {:append_entries, %AppendEntries.Request{term: term, leader_id: leader_id}} = request,
        _from,
        state
      ) do
    current_term = PersistentState.current_term(state.persistent_state)

    if term > current_term do
      Logger.debug(fn ->
        "Raft: Server Leader #{inspect(state.id)} with term: #{inspect(current_term)} got append entries from #{inspect(leader_id)} with newer term: #{inspect(term)}. Converting to Follower and delegating call"
      end)

      state = cleanup(state)
      {:noreply, state, :convert_and_call, Follower, request}
    else
      Logger.debug(fn ->
        "Raft: Server Leader #{inspect(state.id)} with term: #{inspect(current_term)} got append entries from #{inspect(leader_id)} with term: #{inspect(term)}. Fail."
      end)

      response = %AppendEntries.Response{success: false, term: current_term}
      {:reply, response, state}
    end
  end

  def handle_call(
        {:command, command},
        from,
        state
      ) do
    index = PersistentState.append_command(state.persistent_state, command)

    state =
      put_in(
        state.leader_state.pending_commands,
        state.leader_state.pending_commands ++ [{index, from}]
      )

    {:noreply, state}
  end

  def handle_info(
        {:heartbeat_timeout, _},
        state
      ) do
    Logger.debug(fn ->
      "Raft: Server Leader #{inspect(state.id)} got heartbeat timeout. Sending append entries"
    end)

    send_append_entries(state)
    {:noreply, state}
  end

  def handle_info(
        {_task_ref, {:append_entries_response, {:error, _, _}, _peer, _request}},
        state
      ) do
    {:noreply, state}
  end

  @doc """
  Handles AppendEntries failed responses
  """
  def handle_info(
        {_task_ref,
         {:append_entries_response, %AppendEntries.Response{success: false, term: term}, peer,
          request}},
        %{persistent_state: persistent_state, id: id} = state
      ) do
    current_term = PersistentState.current_term(persistent_state)

    cond do
      term > current_term ->
        Logger.debug(fn ->
          "Raft: Server Leader #{inspect(id)} with term: #{inspect(current_term)} got failed entries response with term: #{inspect(term)}. Converting to follower"
        end)

        PersistentState.set_current_term(persistent_state, term)
        {:noreply, cleanup(state), :convert, Follower}

      true ->
        Logger.debug(fn ->
          "Raft: Server Leader #{inspect(id)} with term: #{inspect(current_term)} got failed entries response with term: #{inspect(term)} from peer: #{inspect(Peer.id(peer))}. Decrementing the peer's next_index to: #{inspect(request.prev_log_index)}."
        end)

        peer_id = Peer.id(peer) |> String.to_atom()
        state = put_in(state.leader_state.next_index[peer_id], request.prev_log_index)
        {:noreply, state}
    end
  end

  @doc """
  Handles heartbeat success responses
  """
  def handle_info(
        {_task_ref,
         {:append_entries_response, %AppendEntries.Response{success: true}, _,
          %AppendEntries.Request{entries: []}}},
        state
      ) do
    {:noreply, state}
  end

  @doc """
  Handles AppendEntries success responses
  """
  def handle_info(
        {_task_ref,
         {:append_entries_response, %AppendEntries.Response{success: true}, peer, request}},
        %{
          leader_state: leader_state
        } = state
      ) do
    {_term, _command, last_index} = Enum.at(request.entries, length(request.entries) - 1)
    peer_id = Peer.id(peer) |> String.to_atom()

    leader_state = %{
      leader_state
      | next_index: Map.put(leader_state.next_index, peer_id, last_index + 1),
        match_index: Map.put(leader_state.match_index, peer_id, last_index)
    }

    state = %{state | leader_state: leader_state}
    state = reply_to_command_issuer(state)
    {:noreply, state}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  defp send_append_entries(state) do
    Enum.map(state.peers, fn peer ->
      Task.Supervisor.async_nolink(state.task_supervisor, fn ->
        request = make_append_entries_request_for_peer(state, peer)

        case Peer.append_entries(peer, request) do
          {:ok, response} -> {:append_entries_response, response, peer, request}
          _ -> nil
        end
      end)
    end)
  end

  defp make_append_entries_request_for_peer(
         %{
           persistent_state: persistent_state,
           volatile_state: volatile_state,
           id: id,
           leader_state: leader_state
         },
         peer
       ) do
    peer_id = Peer.id(peer) |> String.to_atom()

    next_index = leader_state.next_index[peer_id]

    entries = PersistentState.entries(persistent_state, next_index)

    current_term = PersistentState.current_term(persistent_state)

    prev_log_index = max(0, next_index - 1)

    prev_log_term = PersistentState.term_of_entry_at(persistent_state, prev_log_index)

    %AppendEntries.Request{
      term: current_term,
      leader_id: id,
      prev_log_index: prev_log_index,
      prev_log_term: prev_log_term,
      entries: entries,
      leader_commit: volatile_state.commit_index
    }
  end

  defp reply_to_command_issuer(
         %{
           peers: peers,
           leader_state: leader_state
         } = state
       ) do
    match_indexes = Map.values(leader_state.match_index)

    last_match_index =
      match_indexes
      |> Enum.filter(fn index ->
        length(Enum.filter(match_indexes, &(&1 >= index))) >= length(peers) / 2
      end)
      |> Enum.max()

    Enum.reduce(
      leader_state.pending_commands,
      state,
      fn {index, reply_to} = pending_command, state ->
        if index <= last_match_index do
          # TODO: apply entry
          GenServer.reply(reply_to, {:ok, index})

          %{
            state
            | volatile_state: %{state.volatile_state | commit_index: index},
              leader_state: %{
                state.leader_state
                | pending_commands:
                    Enum.filter(state.leader_state.pending_commands, &(&1 == pending_command))
              }
          }
        end

        state
      end
    )
  end

  defp cleanup(state) do
    if Timer.running?(state.timers, :heartbeat_timeout, self()) do
      :ok = Timer.stop_timer(state.timers, :heartbeat_timeout, self())
    end

    cancel_running_append_entries(state)
    Map.delete(state, :leader_state)
  end

  defp cancel_running_append_entries(state) do
    Enum.each(state.leader_state.tasks, fn task ->
      Task.shutdown(task, :brutal_kill)
    end)
  end
end
