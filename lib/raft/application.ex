defmodule Raft.Application do
  @moduledoc """

  Supervises a Raft.Server.Supervisor for the server and each peer with `type: :same_node`
  """

  use Application

  def start(_type, _args) do
    raft_id = Application.fetch_env!(:raft, :id)
    peers = Application.fetch_env!(:raft, :peers)

    election_timeout_durations =
      {Application.get_env(:raft, :minimum_election_timeout_duration, 150),
       Application.get_env(:raft, :maximum_election_timeout_duration, 300)}

    cluster = [[id: raft_id, type: :server, options: []]] ++ peers

    children =
      Enum.map(cluster, &server_supervisor(&1, cluster, election_timeout_durations))
      |> Enum.filter(fn child -> not is_nil(child) end)

    Supervisor.start_link(children,
      strategy: :one_for_one,
      type: :supervisor,
      id: Raft.Supervisor
    )
  end

  defp server_supervisor([id: id, type: type, options: _], cluster, election_timeout_durations)
       when type in [:server, :same_node] do
    {Raft.Server.Supervisor,
     [
       id: id,
       peers:
         Enum.filter(cluster, fn [id: peer_id, type: _, options: _] -> peer_id != id end)
         |> Enum.map(&module_for_peer(&1))
         |> Enum.map(&options_for_peer(&1)),
       election_timeout_durations: election_timeout_durations
     ]}
    |> Supervisor.child_spec(id: "#{id}-supervisor")
  end

  defp server_supervisor(_, _, _) do
    nil
  end

  defp module_for_peer(id: id, type: type, options: options)
       when type in [:server, :same_node, :node] do
    [id: id, type: type, module: Raft.Server.Peer.Native, options: options]
  end

  defp module_for_peer(id: id, type: :http, options: options) do
    [id: id, type: :http, module: Raft.Server.Peer.Http, options: options]
  end

  defp options_for_peer(id: id, type: type, module: module, options: _)
       when type in [:same_node, :server] do
    [
      id: id,
      type: type,
      module: module,
      options: [destination: id |> String.to_atom()]
    ]
  end

  defp options_for_peer(peer) do
    peer
  end
end
