defmodule Raft.Server.Follower do
  @moduledoc """
  Provides state and behavior for a Raft Server in Follower state
  """

  require Logger
  alias Raft.Server.{PersistentState, VolatileState, Peer, Candidate}
  alias Raft.Rpc.{AppendEntries, RequestVote}

  def init(%{persistent_state: persistent_state, timers: timers} = state) do
    :ok = Timer.start_timer(timers, :election_timeout, self())
    current_term = PersistentState.current_term(persistent_state)

    Map.put(state, :follower_state, {current_term, nil})
    |> Map.put(:volatile_state, VolatileState.init(persistent_state))
  end

  def handle_call({:command, command}, from, state) do
    case state.follower_state do
      {_, nil} ->
        {:reply, {:error, "Raft Follower: unknown leader"}, state}

      {_, leader} ->
        # Don't block while waiting for leader to reply
        Task.Supervisor.async_nolink(state.task_supervisor, fn ->
          response =
            Peer.command(Enum.find(state.peers, fn peer -> Peer.id(peer) == leader end), command)

          {:command_reply, from, response}
        end)

        {:noreply, state}
    end
  end

  def handle_call(
        {:append_entries,
         %AppendEntries.Request{
           term: term,
           leader_id: leader_id,
           prev_log_index: prev_log_index,
           prev_log_term: prev_log_term,
           entries: entries
         }},
        _from,
        %{persistent_state: persistent_state, id: id, timers: timers} = state
      ) do
    current_term = PersistentState.current_term(persistent_state)

    {reply, state} =
      cond do
        term < current_term ->
          Logger.debug(fn ->
            "Raft: Server Follower #{inspect(id)} with term: #{inspect(current_term)} got append entries from #{inspect(leader_id)} with old term: #{inspect(term)}. Fail."
          end)

          {%AppendEntries.Response{
             term: current_term,
             success: false
           }, state}

        PersistentState.item_with_term_at?(persistent_state, prev_log_index, prev_log_term) ->
          cond do
            term > current_term ->
              Logger.debug(fn ->
                "Raft: Server Follower #{inspect(id)} with term: #{inspect(current_term)} got append entries from #{inspect(leader_id)} with newer term: #{inspect(term)}. Success"
              end)

              :ok = Timer.restart_timer(timers, :election_timeout, self())

              PersistentState.set_current_term_and_append_entries(persistent_state, term, entries)

              {%AppendEntries.Response{
                 term: current_term,
                 success: true
               }, %{state | follower_state: {term, leader_id}}}

            term == current_term ->
              :ok = Timer.restart_timer(timers, :election_timeout, self())

              Logger.debug(fn ->
                "Raft: Server Follower #{inspect(id)} with term: #{inspect(current_term)} got append entries from #{inspect(leader_id)}. Success"
              end)

              index = PersistentState.append_entries(persistent_state, entries)

              {%AppendEntries.Response{
                 term: current_term,
                 success: true
               },
               %{
                 state
                 | follower_state: {term, leader_id},
                   volatile_state: %{state.volatile_state | commit_index: index}
               }}
          end

        true ->
          Logger.debug(fn ->
            "Raft: Server Follower #{inspect(id)} with term: #{inspect(current_term)} got append entries from #{inspect(leader_id)} with the term: #{inspect(term)} but did not find a entry on the log for the index: #{inspect(prev_log_index)} with the term: #{inspect(prev_log_term)}. Fail."
          end)

          :ok = Timer.restart_timer(timers, :election_timeout, self())

          {%AppendEntries.Response{
             term: current_term,
             success: false
           }, state}
      end

    {:reply, reply, state}
  end

  def handle_call(
        {:request_vote, %RequestVote.Request{term: term, candidate_id: candidate_id}},
        _From,
        %{id: id, persistent_state: persistent_state, timers: timers} = state
      ) do
    current_term = PersistentState.current_term(persistent_state)
    voted_for = PersistentState.voted_for(persistent_state)

    {reply, state} =
      cond do
        term < current_term ->
          Logger.debug(fn ->
            "Raft: Server Follower #{inspect(id)} with term: #{inspect(current_term)} got request vote from #{inspect(candidate_id)} with old term: #{inspect(term)}"
          end)

          {%RequestVote.Response{
             term: current_term,
             vote_granted: false
           }, state}

        term > current_term ->
          Logger.debug(fn ->
            "Raft: Server Follower #{inspect(id)} with term: #{inspect(current_term)} got request vote from #{inspect(candidate_id)} with newer term: #{inspect(term)}, granting vote"
          end)

          :ok = Timer.restart_timer(timers, :election_timeout, self())

          PersistentState.set_current_term_and_voted_for(persistent_state, term, candidate_id)

          {%RequestVote.Response{
             term: term,
             vote_granted: true
           }, state}

        voted_for ->
          Logger.debug(fn ->
            "Raft: Server Follower #{inspect(id)} with term: #{inspect(current_term)} got request vote from #{inspect(candidate_id)} but already voted for #{inspect(voted_for)}"
          end)

          {%RequestVote.Response{
             term: current_term,
             vote_granted: false
           }, state}

        term == current_term ->
          Logger.debug(fn ->
            "Raft: Server Follower #{inspect(id)} with term: #{inspect(current_term)} granting vote to #{inspect(candidate_id)}"
          end)

          :ok = Timer.restart_timer(timers, :election_timeout, self())

          PersistentState.set_voted_for(persistent_state, candidate_id)

          {%RequestVote.Response{
             term: term,
             vote_granted: true
           }, state}
      end

    {:reply, reply, state}
  end

  def handle_info({_task_ref, {:command_reply, reply_to, response}}, state) do
    GenServer.reply(reply_to, response)
    {:noreply, state}
  end

  @doc """
  Convert to candidate if no append_entries were received from current leader and if no votes were granted to candidate
  """
  def handle_info(
        {:election_timeout, _},
        %{id: id, persistent_state: persistent_state} = state
      ) do
    current_term = PersistentState.current_term(persistent_state)

    Logger.debug(fn ->
      "Raft: Server Follower #{inspect(id)} with term: #{inspect(current_term)} got election timeout. Converting to candidate"
    end)

    {:noreply, cleanup(state), :convert, Candidate}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  defp cleanup(state) do
    Map.delete(state, :follower_state)
  end
end
