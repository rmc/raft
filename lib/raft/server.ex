defmodule Raft.Server do
  use GenServer

  require Logger

  alias Raft.Server.{Follower}
  alias Raft.Timeout

  defmodule(Config,
    do:
      defstruct(
        id: "",
        peers: [],
        persistent_state: nil,
        minimum_election_duration: 150,
        maximum_election_duration: 300
      )
  )

  def start_link(config) do
    GenServer.start_link(__MODULE__, config, name: config.id |> String.to_atom())
  end

  def append_entries(
        server,
        request
      ) do
    GenServer.call(server, {:append_entries, request})
  end

  def request_vote(server, request) do
    GenServer.call(server, {:request_vote, request})
  end

  def command(server, command) do
    GenServer.call(server, {:command, command})
  end

  # Server

  def init(%Raft.Server.Config{
        id: id,
        peers: peers,
        persistent_state: persistent_state,
        minimum_election_duration: minimum_election_duration,
        maximum_election_duration: maximum_election_duration
      }) do
    election_duration =
      Timeout.new_election_duration(
        minimum_election_duration,
        maximum_election_duration
      )

    heartbeat_duration = Timeout.broadcast_time_duration(election_duration)
    Logger.info("election timeout duration for server #{id}: #{election_duration}")
    Logger.info("heartbeat timeout duration for server #{id}: #{heartbeat_duration}")

    {:ok, timers} = Timer.start_link()

    :ok =
      Timer.register_timers(
        timers,
        [
          %Timer.Config{
            id: :election_timeout,
            duration: election_duration
          },
          %Timer.Config{
            id: :heartbeat_timeout,
            duration: heartbeat_duration
          }
        ],
        self()
      )

    {:ok, task_supervisor} =
      Task.Supervisor.start_link(name: "#{id}-task-supervisor" |> String.to_atom())

    state =
      convert(Follower, %{
        id: id,
        peers: peers,
        persistent_state: persistent_state,
        timers: timers,
        task_supervisor: task_supervisor
      })

    {:ok, state}
  end

  def handle_call(request, from, state) do
    state.server.handle_call(request, from, state) |> handle_server_implementation_response()
  end

  def handle_info(request, state) do
    state.server.handle_info(request, state) |> handle_server_implementation_response()
  end

  defp handle_server_implementation_response({:reply, reply, state, :convert, to}) do
    {:reply, reply, convert(to, state)}
  end

  defp handle_server_implementation_response({:noreply, state, :convert, to}) do
    {:noreply, convert(to, state)}
  end

  defp handle_server_implementation_response({:noreply, state, :convert_and_call, to, req}) do
    state = convert(to, state)
    to.handle_call(req, nil, state)
  end

  defp handle_server_implementation_response(response) do
    response
  end

  defp convert(to, state) do
    Map.put(state, :server, to)
    |> to.init
  end
end
