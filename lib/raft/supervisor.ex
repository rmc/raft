defmodule Raft.Server.Supervisor do
  use Supervisor

  def start_link([
        {:id, id},
        {:peers, peers},
        {:election_timeout_durations, election_timeout_durations} | opts
      ]) do
    Supervisor.start_link(__MODULE__, {id, peers, election_timeout_durations}, opts)
  end

  def init({
        id,
        peers,
        {minimum_election_duration, maximum_election_duration}
      }) do
    peers = Enum.map(peers, &peer_id_to_process_name(&1, id))
    persistent_state_id = "#{id}-persistent_state" |> String.to_atom()

    persistent_state =
      {Raft.Server.PersistentState, [name: persistent_state_id]}
      |> Supervisor.child_spec(id: persistent_state_id)

    server =
      {Raft.Server,
       %Raft.Server.Config{
         id: id,
         peers: peers,
         persistent_state: persistent_state_id,
         maximum_election_duration: maximum_election_duration,
         minimum_election_duration: minimum_election_duration
       }}
      |> Supervisor.child_spec(id: id |> String.to_atom())

    children = Enum.map(peers, &peer_child(&1)) ++ [persistent_state, server]
    opts = [strategy: :one_for_one]
    Supervisor.init(children, opts)
  end

  defp peer_id_to_process_name([id: id, type: type, module: module, options: options], server_id)
       when module in [Raft.Server.Peer.Native] do
    name = "#{server_id}-peer:#{id}" |> String.to_atom()

    [id: id, name: name, type: type, module: module, options: options]
  end

  defp peer_child(id: _, name: name, type: _, module: module, options: options)
       when module in [Raft.Server.Peer.Native] do
    {module, [name: name] ++ options}
    |> Supervisor.child_spec(id: name)
  end
end
