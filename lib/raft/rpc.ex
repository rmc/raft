defmodule Raft.Rpc.RequestVote do
  @moduledoc """
  Invoked by candidates to gather votes (§5.2).
  """

  defmodule Request do
    @moduledoc """
    term: candidate's term
    candidate_id: candidate requesting vote
    last_log_index: index of candidate's last log entry (§5.4)
    last_log_term: term of candidate’s last log entry (§5.4)
    """
    defstruct [:term, :candidate_id, :last_log_index, :last_log_term]
  end

  defmodule Response do
    @moduledoc """
    """
    defstruct [:term, :vote_granted]
  end
end

defmodule Raft.Rpc.AppendEntries do
  @moduledoc """
  Invoked by leader to replicate log entries (§5.3).
  Also used as heartbeat (§5.2).
  """

  defmodule Request do
    @moduledoc """
    term: leader's term
    leader_id: so follower can redirect clients
    prev_log_index: index of log entry immediately preeeding new ones
    prev_log_term: term of pre_log_index entry
    entries: log entries to store (empty for heartbeat; may send more than one for efficiency)
    leader_commit: leader's commit index
    """
    defstruct [:term, :leader_id, :prev_log_index, :prev_log_term, :leader_commit, entries: []]
  end

  defmodule Response do
    defstruct [:term, :success]
  end
end
