defmodule Raft.Timeout do
  @moduledoc """

  Exposes functions for calculating timeout durations.

  broadcastTime ≪ electionTimeout ≪ MTBF

  - broadcastTime: the average time it takes a server to send RPCs in parallel to every server in the cluster and receive their responses
  - electionTimeout:
  - MTBF: the average time between failures for a single server.

  The broadcast time should be an order of magnitude less than the election timeout so that leaders careliably send the heartbeat messages required to keep followers from starting elections;
  """
  @spec new_election_duration(integer(), integer()) :: integer()
  def new_election_duration(
        minimum_election_timeout_duration \\ 150,
        maximum_election_timeout_duration \\ 300
      ) do
    Enum.random(minimum_election_timeout_duration..maximum_election_timeout_duration)
  end

  @spec broadcast_time_duration(integer()) :: integer()
  def broadcast_time_duration(election_timeout_duration \\ new_election_duration()) do
    (election_timeout_duration / 10) |> round()
  end
end
