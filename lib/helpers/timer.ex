defmodule Timer do
  use GenServer

  defmodule(Config, do: defstruct([:id, :duration]))

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def register_timers(timer, timers, from) do
    GenServer.call(timer, {:register_timers, timers, from})
  end

  def unregister_timers(timer, timers, from) do
    GenServer.call(timer, {:unregister_timers, timers, from})
  end

  def start_timer(timer, timer_id, from) do
    GenServer.call(timer, {:start_timer, timer_id, from})
  end

  def restart_timer(timer, timer_id, from) do
    GenServer.call(timer, {:restart_timer, timer_id, from})
  end

  def stop_timer(timer, timer_id, from) do
    GenServer.call(timer, {:stop_timer, timer_id, from})
  end

  def running?(timer, timer_id, from) do
    GenServer.call(timer, {:running?, timer_id, from})
  end

  # Server
  def init(:ok) do
    ref = make_ref()
    {:ok, %{ref: ref, listeners: %{}}}
  end

  def handle_call({:register_timers, timers, from}, _from, state) do
    {:reply, :ok,
     %{
       state
       | listeners:
           Map.put(
             state.listeners,
             from,
             List.foldl(timers, %{}, fn %Config{id: id, duration: duration}, acc ->
               Map.put(acc, id, %{id: id, duration: duration, timer_ref: nil})
             end)
           )
     }}
  end

  def handle_call({:unregister_timers, _timers, from}, _from, state) do
    # TODO: unregister specific timers for a listener?
    {:reply, :ok,
     %{
       state
       | listeners: Map.delete(state.listeners, from)
     }}
  end

  def handle_call({:running?, timer_id, from}, _from, state) do
    {:reply, not is_nil(state.listeners[from][timer_id].timer_ref), state}
  end

  def handle_call({:start_timer, timer_id, from}, _from, state) do
    {:reply, :ok,
     %{
       state
       | listeners:
           Map.update!(state.listeners, from, fn timers ->
             Map.update!(timers, timer_id, fn timer ->
               %{timer | timer_ref: schedule_timeout(timer.duration, state.ref, from, timer.id)}
             end)
           end)
     }}
  end

  def handle_call({:restart_timer, timer_id, from}, _from, state) do
    {:reply, :ok,
     %{
       state
       | listeners:
           Map.update!(state.listeners, from, fn timers ->
             Map.update!(timers, timer_id, fn timer ->
               Process.cancel_timer(timer.timer_ref)
               %{timer | timer_ref: schedule_timeout(timer.duration, state.ref, from, timer_id)}
             end)
           end)
     }}
  end

  def handle_call({:stop_timer, timer_id, from}, _from, state) do
    {:reply, :ok,
     %{
       state
       | listeners:
           Map.update!(state.listeners, from, fn timers ->
             Map.update!(timers, timer_id, fn timer ->
               Process.cancel_timer(timer.timer_ref)
               %{timer | timer_ref: nil}
             end)
           end)
     }}
  end

  def handle_info({:timeout, duration, ref, from, timer_id}, %{ref: state_ref} = state)
      when ref == state_ref do
    {:noreply,
     %{
       state
       | listeners:
           Map.update!(
             state.listeners,
             from,
             fn timers ->
               Map.update!(timers, timer_id, fn timer ->
                 send(from, {timer_id, Time.utc_now()})

                 Map.put(
                   timer,
                   :timer_ref,
                   schedule_timeout(duration, ref, from, timer_id)
                 )
               end)
             end
           )
     }}
  end

  defp schedule_timeout(duration, ref, from, timer_id) do
    Process.send_after(self(), {:timeout, duration, ref, from, timer_id}, duration)
  end
end
