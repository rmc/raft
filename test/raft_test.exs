defmodule Raft.ServerTest do
  use ExUnit.Case, async: true

  setup do
    peers = [:peer, module: Raft.Server.Peer.Native, options: []]
    start_supervised!({Raft.Server.Peer.Native, [name: :peer, destination: self()]})
    persistent_state = start_supervised!(Raft.Server.PersistentState)

    {:ok, pid} =
      Raft.Server.start_link(%Raft.Server.Config{
        id: "test",
        peers: peers,
        persistent_state: persistent_state
      })

    {:ok, server: pid, peer: :peer}
  end

  test "it starts as a follower", %{server: server} do
    request = %Raft.Rpc.RequestVote.Request{
      term: 1,
      candidate_id: "test",
      last_log_index: 0,
      last_log_term: 0
    }

    %Raft.Rpc.RequestVote.Response{term: term, vote_granted: vote_granted} =
      Raft.Server.request_vote(server, request)

    assert term == 1
    assert vote_granted == true
  end
end
